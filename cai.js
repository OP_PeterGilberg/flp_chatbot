sap.ui.define([], () => {
    let proxy = {
        getMemory: null,
        onMessage: null
    }
    window.sapcai = { 
        src: "https://cdn.cai.tools.sap/webclient/bootstrap.js",
        id: "cai-webclient-custom",
        final_id: "cai-webclient-builtin-button",
        webclientBridge: {

            getMemory: () => {
                if (proxy.getMemory != null)
                    return proxy.getMemory()
                else
                    return {memory: {}, merge: true}
            },

            onMessage: (payload) => {
                console.log(`cai proxy: onMessage ${payload}`)
                if (proxy.onMessage !== null)
                    proxy.onMessage(payload)
            }

        }
    }
    return {
        
        appendScript: (src) => {
            var objSrc = src
            if (typeof(src) == 'string') {
                objSrc = {}
                let dom = new DOMParser().parseFromString(src, 'text/html')
                let scripts = dom.getElementsByName("script")
                if (scripts.length != 1) 
                    throw "no/too many <script>-tags specified"
                let script = scripts[0]
                objSrc.channeldId = script.getAttribute('data-channel-id')
                if(!objSrc.channeldId)
                    throw "missing attribute: data-channel-id: ${channeldId}"
                objSrc.token = script.getAttribute('data-token')
                if (!objSrc.token)
                    throw `missing attribute: data-token: ${token}`
                objSrc.preferences = script.getAttribute('data-expander-preferences')
                if (!objSrc.preferences)
                    throw `missing attribute: data-expander-preferences: ${preferences}`
            } else if (typeof(src) !== 'object' || !objSrc.channeldId || !objSrc.token || !objSrc.preferences) {
                throw `missing attributes in src obj: ${obj}`
            }

            if (!document.getElementById(window.sapcai.id)) {
                let s = document.createElement('script')
                s.setAttribute('id', window.sapcai.id)
                s.setAttribute('src', window.sapcai.src)
                s.setAttribute('data-expander-type', "CAI"),
				s.setAttribute('data-channel-id', objSrc.channeldId);
				s.setAttribute('data-token', objSrc.token);
                s.setAttribute('data-expander-preferences', objSrc.preferences)
                document.body.append(s)
            } else {
                throw 'cai script already appended!'
            }
        },

        defineHook: (name, handler) => {
            if (proxy[name] === undefined) {
                console.error(`Hook ${name} is not supported`)
            }
            proxy[name] = handler
        },

        client: () => window.sap.cai.webclient,

        msg: function(str) {
            this.client().sendMessage(str)
        },

        toggle: function() {
            this.client().toggle()
        },

        destroy: function() {
            // HACK
            this.client().hide()
            let doc = document.getElementById(window.sapcai.final_id)
            if (doc) 
                doc.remove()
        }
    }
})
