 sap.ui.define([
	"sap/ui/core/Component",
	"sap/m/Button",
	"sap/m/Bar",
	"sap/m/MessageToast",
    "./cai"
], function (Component, Button, Bar, MessageToast, CAI) {
	
	return Component.extend("de.orbispeole.flp.chatbot.Component", {

		metadata: {
			"manifest": "json"
		},

		init: function () {

			var rendererPromise = this._getRenderer();

            const chatbot = {
                name: "flp_demo_bot",
                channeldId: "f207aa5f-7de7-421e-8516-4dac72b727c3",
                token: "25ecbed3ba7e3fa64bb3b0698f44252e",
                preferences: "JTdCJTIyZXhwYW5kZXJMb2dvJTIyJTNBJTIyaHR0cHMlM0ElMkYlMkZjZG4uY2FpLnRvb2xzLnNhcCUyRndlYmNoYXQlMkZ3ZWJjaGF0LWxvZ28uc3ZnJTIyJTJDJTIyZXhwYW5kZXJUaXRsZSUyMiUzQSUyMkFzc2lzdGVudCUyMCVDMyVCNmZmbmVuJTIyJTJDJTIyb25ib2FyZGluZ01lc3NhZ2UlMjIlM0ElMjJCZWVwJTIwYm9wJTJDJTIwa2FubiUyMGljaCUyMGJlaGlsZmxpY2glMjBzZWluJTNGJTIyJTJDJTIyb3BlbmluZ1R5cGUlMjIlM0ElMjJuZXZlciUyMiUyQyUyMnRoZW1lJTIyJTNBJTIyREVGQVVMVCUyMiU3RA==",
                auth: {
                    url: "https://sapcai-community.authentication.eu10.hana.ondemand.com/oauth/token",
                    client_id: "sb-66327ac9-8660-4a6b-bada-b284263dc0ce-CLONE-BS-RT!b40741|cai-production!b20881",
                    client_secret: "binding-66327ac9-8660-4a6b-bada-b284263dc0ce$EH7oDdb5TzWQmQ1Mn4SsZYti2Oozjj3pwAa0q2oXYDQ="
                },

                getAuth: async function() {
                    let resp = await fetch(this.auth.url, {
                        method: 'POST',
                        header: [ 'Authentication', 'Basic' + btoa(this.auth.client_id + ':' + this.auth.client_secret)],
                        body: "{'grant_type' : 'client_credentials'}"
                    })
                    let body = await resp.json()
                    console.log(body)
                    return resp                
                },

                sendMsg: async function(msg) {
                    let tok = await this.auth.tok
                    console.log(`TODO: sendMsg ${msg}`)
                },

                init: function() {
                    CAI.appendScript(this)
                    this.auth.tok = this.getAuth()
                    // XXX: Remove
                    window._bot = this
                }
            }

			const Intent = function(so, act) { return { sem_obj: so, action: act } };
			
            const cai2flp_intent = {
	            'call-in-sick': new Intent('LeaveRequest', 'manage'),
	        	'requset-payslip': new Intent('RemunerationStatement', 'display'),
	        	'manage-timesheet': new Intent('TimeEntry', 'manageTimesheet')
            }

            const navTo = function(semObj, action) {
                this.crossAppNav = sap.ushell.Container.getService("CrossApplicationNavigation")
                let shellHash = (this.crossAppNav && this.crossAppNav.hrefForExternal({
                    target: {
                        semanticObject: semObj,
                        action: action
                    },
                    params: {
                    }
                })) || "";

                this.crossAppNav.isIntentSupported([shellHash])
                .done(function (aResponses) {
                    if (aResponses[shellHash].supported === true) {
                        this.crossAppNav.toExternal({
                            target: {
                                shellHash: shellHash
                            }
                        });
                    } else {
                        console.debug("Intent not supported"); //@Hack
                    }
                }.bind(this))
                .fail(function () {
                    console.debug("Intent not supported"); //@Hack
                }.bind(this));
            }

            chatbot.init()
            
            CAI.defineHook("getMemory", function() {
            	console.log("getMemory")
            	return { memory: ({cai2flp: cai2flp_intent}), merge: true}
            })

            CAI.defineHook("onMessage", function(payload) {
            	payload.messages.forEach(msg => {
                    if (!this.convId || this.convId !== msg.conversation) {
                        console.debug(`convId: ${this.convId} -> ${msg.conversation}`)
                        this.convId = msg.conversation
                    }
            		if (msg.attachment.type !== 'client_data')
            			return;
            		let args = msg.attachment.content.elements
            		let cmd = args[0]
            		console.assert(cmd.key === 'cmd')
            		switch (cmd.value) {
            			case 'nav_to': {
            				console.log(args[1].valueOf())
            				debugger
            				navTo(args[1].value.sem_obj, args[1].value.action)
            				CAI.toggle()
            			}
            		}
            	})
            })
		},

		/**
		 * Returns the shell renderer instance in a reliable way,
		 * i.e. independent from the initialization time of the plug-in.
		 * This means that the current renderer is returned immediately, if it
		 * is already created (plug-in is loaded after renderer creation) or it
		 * listens to the &quot;rendererCreated&quot; event (plug-in is loaded
		 * before the renderer is created).
		 *
		 *  @returns {object}
		 *      a jQuery promise, resolved with the renderer instance, or
		 *      rejected with an error message.
		 */
		_getRenderer: function () {
			var that = this,
				oDeferred = new jQuery.Deferred(),
				oRenderer;

			that._oShellContainer = jQuery.sap.getObject("sap.ushell.Container");
			if (!that._oShellContainer) {
				oDeferred.reject(
					"Illegal state: shell container not available; this component must be executed in a unified shell runtime context.");
			} else {
				oRenderer = that._oShellContainer.getRenderer();
				if (oRenderer) {
					oDeferred.resolve(oRenderer);
				} else {
					// renderer not initialized yet, listen to rendererCreated event
					that._onRendererCreated = function (oEvent) {
						oRenderer = oEvent.getParameter("renderer");
						if (oRenderer) {
							oDeferred.resolve(oRenderer);
						} else {
							oDeferred.reject("Illegal state: shell renderer not available after recieving 'rendererLoaded' event.");
						}
					};
					that._oShellContainer.attachRendererCreatedEvent(that._onRendererCreated);
				}
			}
			return oDeferred.promise();
		}
	});
});
